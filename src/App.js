import React, { useState } from "react";
import i18n from "./i18n";
import { withNamespaces } from "react-i18next";
import { LANUAGES } from "./utils/constants";

function App(props) {
  const [languageCode, setLanguageCode] = useState("en");
  const { t } = props;

  const changeLanguageHandler = e => {
    const { value } = e.target;
    setLanguageCode(value);
    i18n.changeLanguage(value);
  };

  return (
    <div style={{ textAlign: "center", marginTop: 20 }}>
      <div>
        <div>
          <select value={languageCode} onChange={changeLanguageHandler}>
            {LANUAGES.map(({ code, name }, key) => {
              return (
                <option key={key} value={code}>
                  {name}
                </option>
              );
            })}
          </select>
        </div>
      </div>
      <h1>{t("Welcome to Language Translation Sample Application")}</h1>
      <h3>{t("What language do you prefer to read with?")}</h3>
    </div>
  );
}

export default withNamespaces()(App);
