import i18n from "i18next";
import { reactI18nextModule } from "react-i18next";
import translationEN from "../src/locales/en/translation.json";
import translationAR from "../src/locales/ar/translation.json";
import translationZH from "../src/locales/zh-Hans/translation.json";
import translationSI from "../src/locales/si/translation.json";
import translationCS from "../src/locales/cs/translation.json";
import translationDA from "../src/locales/da/translation.json";
import translationNL from "../src/locales/nl/translation.json";
import translationFI from "../src/locales/fi/translation.json";
import translationFR from "../src/locales/fr/translation.json";
import translationKA from "../src/locales/ka/translation.json";
import translationDE from "../src/locales/de/translation.json";
import translationEL from "../src/locales/el/translation.json";
import translationHI from "../src/locales/hi/translation.json";
import translationGA from "../src/locales/ga/translation.json";
import translationIT from "../src/locales/it/translation.json";
import translationJA from "../src/locales/ja/translation.json";
import translationKO from "../src/locales/ko/translation.json";
import translationLA from "../src/locales/la/translation.json";
import translationSK from "../src/locales/sk/translation.json";
import translationES from "../src/locales/es/translation.json";
import translationSU from "../src/locales/su/translation.json";
import translationSV from "../src/locales/sv/translation.json";
const resources = {
  en: {
    translation: translationEN
  },
  ar: {
    translation: translationAR
  },
  zh: {
    translation: translationZH
  },
  si: {
    translation: translationSI
  },
  cs: {
    translation: translationCS
  },
  da: {
    translation: translationDA
  },
  nl: {
    translation: translationNL
  },
  fi: {
    translation: translationFI
  },
  fr: {
    translation: translationFR
  },
  ka: {
    translation: translationKA
  },
  de: {
    translation: translationDE
  },
  el: {
    translation: translationEL
  },
  hi: {
    translation: translationHI
  },
  ga: {
    translation: translationGA
  },
  it: {
    translation: translationIT
  },
  ja: {
    translation: translationJA
  },
  ko: {
    translation: translationKO
  },
  la: {
    translation: translationLA
  },
  sk: {
    translation: translationSK
  },
  es: {
    translation: translationES
  },
  su: {
    translation: translationSU
  },
  sv: {
    translation: translationSV
  }
};

i18n.use(reactI18nextModule).init({
  resources,
  lng: "en",

  keySeparator: false,

  interpolation: {
    escapeValue: false
  }
});

export default i18n;
